# Inclusión Electrónica y Tecnologías Libres

**Author:** Pavel Ernesto Vázquez Martínez

**Contact:** pavel_e [at] riseup.net

**Date:** 10/11/2020


## Objetivo General

Vincular de forma consciente los términos *inclusión*, *electrónica* y *tecnologías libres* a través del desarrollo y la implementación de un módulo que permita a las personas con baja visión y sin discapacidad visual familizarizarse con el sistema de lectoescritura Braille.

## Objetivos particulares

* Conocer los conceptos de inclusión, barreras y accesibilidad para las Personas con Discapacidad.

* Conocer herramientas libres que permitan diseñar e implementar un módulo que
facilite el aprendizaje de la lectoescritura braille.

* Soldadura SMD

* Conocer estrategias y herramientas para publicar y compartir hardware y software libre.

## Inclusión

La inclusión es el paradigma más actual en torno a la relación social en cuanto a las Personas con Discapacidad (PCD) se refiere. En este paradigma las PCD son consideradas sujetos de derechos.

Algunos paradigmas anteriores como, la integración, la segregación e incluso la exclusión aún se encuentran latentes en nuestra sociedad.

![Imagen Inclusion untegracion](img/inclusion-integracion.png "Imagen que muestra los paradigmas de inclusión, integración, segregación, exclusión.")

## Barreras y Discapacidad

La Convención sobre los Derechos de las Personas con Discapacidad es un documento generado en la ONU, firmado y ratificado por México. Este hecho coloca a la convención en un rango de nivel Constitucional.

La Convención establece que la discapacidad surge de la interacción entre las personas y las barreras del entorno e ideológicas que le impiden desarrollarse en igualdad de circunstancias.

Las barreras del entorno se derivan de las ideologías con las que se construyó la sociedad.

Así en la actualidad podemos decir que las barreras más difíciles de erradicar son las actitudinales o ideológicas.

## Accesibilidad

La accesibilidad consiste en eliminar las barreras para que las PCD puedan desarrollarse en igualdad de circunstancias. Esto se logra por medio de los Ajustes razonables y el Diseño Universal.

### Ajustes razonables

Son aquellas "modificaciones y adaptaciones necesarias y adecuadas que no impongan una carga desproporcionada o indebida cuando se requieran en un caso particular" [1] a fin de garantizar todos los derechos y libertades de las PCD.

### Diseño universal
Es el diseño de productos, programas, entornos o servicios "que puedan utilizar todas las personas en la mayor medida posible sin necesidad de adaptación ni diseño especializado"

El Diseño universal no excluye las ayudas técnicas para grupos particulares de PCD.

## Lectoescritura en la Discapacidad visual

Muchas de las PCDV utilizan el sistema de lectoescritura Braille, el cual genera los caracteres a partir de un cuadratín (una matriz de puntos de tres renglones y dos columnas).

![Sistema Braille tradicional](img/sistemaBraille.png "imagen con las combinaciones básicas para construir los caracteres en el sistema braille.")

En el caso de la discapacidad se tienen dos grandes rubros: la ceguera y la baja visión, en el caso de la baja visión es posible percibir algunos colores con altos contraste.

# Diseño de un módulo Braille para arduino

En el programa PILARES de la CDMX se tiene contemplada de forma transversal la inclusión de las Personas con Discapacidad, Personas de la Diversidad Sexual y con un enfoque además Intercultural.

Pare acercarnos a esos objetivos es importante que las personas estén preparadas para integrar y atender las necesidades de estas poblaciones.

## Software y Hardware Libres

El ecosistema actual  de  software y hardware libres tienen su origen en la filosofía planteada por Richard Stallman.

Esta filosofía está basada en 4 libertades:

* **Libertad 0:** La libertad de usar el programa, con cualquier propósito (uso).
* **Libertad 1:** La libertad de estudiar cómo funciona el programa y modificarlo, adaptándolo a las propias necesidades (estudio).
* **Libertad 2:** La libertad de distribuir copias del programa, con lo cual se puede ayudar a otros usuarios (distribución).
* **Libertad 3:** La libertad de mejorar el programa y hacer públicas esas mejoras a los demás, de modo que toda la comunidad se beneficie (mejora).

![Libertades del Software Libre](img/4libertades.png "Imagen de circunferencia dividida en cuatro. Cada cuarto tiene un texto: Use, Share, Study, Improve.")

## Diseño Electrónico Automatizado & KiCAD

KiCAD es una de las aplicaciones más populare en el terreno del Software Libre para diseñar y visualicar PCBSs, también permite simular circuitos con modelos SPICE vía NGSpice.

![Logo CERN](img/kicadLogo.png)

En la actualidad la Organizacion Europea de Investigacion Nuclear (European Organization for Nuclear Research) utiliza KiCAD en el desarrollo de sus investigaciones.

![Logo CERN](img/cernLogo.png)

## Diseño del Módulo Cuadratín para Arduino

En el git [braille_smd](https://gitlab.com/dpw19/braille_smd) se encuentran todos los archivos necesarios para satisfacer las 4 libertades, incluidos los archivos gerber para la producción en masa.

Algunas imágenes sobre el diseño del modulo cuadratin braille_smd

![Cuadratin smd front](https://gitlab.com/dpw19/braille_smd/-/raw/master/CuadratinBraille_smd/img/01braille_smd.png)

![Cuadratin smd botom](https://gitlab.com/dpw19/braille_smd/-/raw/master/CuadratinBraille_smd/img/02braille_smd.png)

![Cuadratin smd esquemético](https://gitlab.com/dpw19/braille_smd/-/raw/master/CuadratinBraille_smd/img/03braille_smd.png)

![Cuadratin smd front](https://gitlab.com/dpw19/braille_smd/-/raw/master/CuadratinBraille_smd/img/cuadratin4.png)

![Cuadratin smd front](https://gitlab.com/dpw19/braille_smd/-/raw/master/CuadratinBraille_smd/img/cuadratin5.png)

![Cuadratin smd front](https://gitlab.com/dpw19/braille_smd/-/raw/master/CuadratinBraille_smd/img/cuadratin6.png)

![Cuadratin smd front](https://gitlab.com/dpw19/braille_smd/-/raw/master/CuadratinBraille_smd/img/cuadratin7.png)

## Montaje y soldadura de componentes con stencil

La soldadura de componentes de SMD requiere de forma ideal un stencil para la aplicación de la soldadura en pasta, montaje automatizado de componentes y un horno con control de temperatura.

El stencil puede ser de diversos materiales en este caso se utiliza uno de aluminio

![Cuadratin smd front](img/IMG_20201018_152856.jpg "Stencil de aliminio y soldadura en pasta")

![Cuadratin smd front](img/IMG_20201018_153005.jpg "Stencil de aluminio con soldadura en pasta aplicada")

Una vez que los pads del PCB tienen la soldadura se colocan los componentes en su sitio lo más alineados posibles.

La forma ideal de fundir la soldadura es con un horno con control de temperatura. En este caso se realizo con una pistola de aire caliente de baja presión.

![Cuadratin smd front](http://microensamble.com/wp-content/uploads/2016/02/2017-05-18_105601.jpg "Imagen de http://microensamble.com/perfil-de-temperatura-circuitos-impresos/")

## Código Arduino

Cuando los componenentes se han soldado podemos verificar el funcionamiento del modulo en una tarjeta de desarrollo Arduino UNO con el siguiente código

```
// Ultima modificacion 10/11/2020

// Código de prueba para el cuadratin braille smd
// Puedes encontrar el esquemático y pcb y este
// código en gitlab.com/dpw/brailleSMD
//
// Este programa es software libre, puedes utilizarlo, estudiarlo, mejorarlo,
// compartirlo bajo los términos de la Licencia Publica General GNU/GPL
//

// Pines del cuadratin braille
const int pins[6] = {8,9,10,11,12,13};

// Codificacion de los caracteres braille
// To Do: Terminar la tabla
const byte puntosCuadratin[10] = {
                          0b100000, // a
                          0b110000, // b
                          0b100100, // c
                          0b100110, // d
                          0b100010, // e     
                          0b110100, // f
                          0b110110, // g
                          0b110010, // h
                          0b010100, // i
                          0b010110, // j
};


void setup() {
// Inicializa los pines del cuadratin como salida
  for(int i = 0; i < 6; i++) {
    pinMode(pins[i], OUTPUT);  
  }

// Apaga los segmentos del cuadratin
  lightSegments(0);
}

void loop() {
// recorre la matriz punto cuadratin
  for(int i = 0; i < 10; i++) {
    lightSegments(i);
    delay(1000);
  }
}

void lightSegments(int number) {
  byte numberBit = puntosCuadratin[number];
  for (int i = 0; i < 6; i++)  {
    int bit = bitRead(numberBit, i);
    digitalWrite(pins[i], bit);
  }
}
```

## Cómo compartir y colaborar

Una forma moderna para compartir y/o colaborar en proyectos de diversa índole es por medio de [Git](https://git-scm.com).

Git es el sistema de control de versiones más moderno y popularizado.

Basicamente consta de dos repositorios uno público y uno privado.

El repositorio público puede clonarse para convertirlo en un repositorio local.

Una vez que haz hecho tus aportaciones puedes solicitar un Merge para que estas sean publicadas.

Aunque todo se puede hacer por linea de comandos existen una gran cantidad de frontends que facilitan su uso.

### Cómo clonar

para clonar un repositorio publico basta con conocer la dirección del repositorio de interés.

El siguiente comando clonaría este repositorio a tu espacio de trabajo local

```
git clone gitlab.com/dpw19/InclusionElectronicaTecnologiasLibres
```

### Cómo trabajo en mi repositorio local

Una vez que se han realizado todos los ajustes o aportaciones en el repositorio local este debe prepararse para ello

```
git clone add [archivo1 | carpeta1 | ...]
git commit -m "Mensaje de la aportacion"
```

### Cómo publico una aportación en un repo clonado

Para publicar una aportacion es necesario tener una cuenta en algún servidor git.

Lo más común es generar una nueva rama del proyecto, trabajar sobre ella y despues realizar un a peticion de unión

Si ya perteneces aun proyecto git basta con

```
git clone add [archivo1 | carpeta1 | ...]
git commit -m "Mensaje de la aportacion"
git push origin master
```

## Implementaciones git
Git es el nombre de la tecnología de control de versiones, que por cierto es software libre.

Existen diversas implementaciones públicas que ofrecen el servicio git, las más populares son [gitlab](gitlab.com) y [github](github.com).


## To Do

### Inclusión
1. Publicar videos en medio de libre acceso.
1. Agregar interepretación LSM a videos.
1. Agregar subtítulos a videos.

### Hardware
1. Pasar del modo de prototipo al modo de producción.
  1. Incluir MCU en el PCB.
  1. Incluir puerto USB en el PCB.
  1. ¿Batería?

### Software
1. Refinar el código para entrenamiento braille
  1. Lectura de teclado
  1. Traducción a Braille
1. Interface/App para entrenamiento braille
1. Modo entrenamiento, con niveles de aprendizaje
1. Modo captura

### Otros proyectos
Teclado tipo Perkins
Integración con cuadratin craille
